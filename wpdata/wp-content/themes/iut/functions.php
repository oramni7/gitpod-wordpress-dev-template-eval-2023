<?php
add_action( 'wp_enqueue_scripts', 'iut_enqueue_styles' );
function iut_enqueue_styles() {
	$parenthandle = 'twentynineteen-style';
	$theme = wp_get_theme();
	wp_enqueue_style( 
        $parenthandle,
		get_template_directory_uri() . '/style.css',
		array(),  // If the parent theme code has a dependency, copy it to here.
		$theme->parent()->get( 'Version' )
	);
	wp_enqueue_style( 'child-style',
		get_stylesheet_uri(),
		array( $parenthandle ),
		$theme->get( 'Version' ) // This only works if you have Version defined in the style header.
	);
}

function iut_recipe_post_type() {
	register_post_type('iut_recipe',
		array(
			'labels'      => array(
				'name'          => 'recette',
				'singular_name' => 'recette',
			),
			'public'      => true,
			'has_archive' => 'recettes',
			'rewrite'     => array( 'slug' => 'recette' ), // my custom slug
            'show_in_rest' => true,
		)
	);
}
add_action('init', 'iut_recipe_post_type');


function iut_add_meta_boxes_recette( $post ) {

	add_meta_box(
		'iut_mbox_recette',                // Unique ID
		'Infos complémentaires',  // Box title
		'iut_mbox_recette_content', 		// Content callback, must be of type callable
		'recette'                          	// Post type
	);

}

add_action( 'add_meta_boxes', 'iut_add_meta_boxes_recette' );

function iut_mbox_recette_content( $post ) {

	// Get meta value
	$ingredients = get_post_meta(
		$post->ID,
		'iut-ingredients',
		true
	);
	echo '<p>';
	echo '<label for="iut-ingredients">';
	echo 'Ingredients';
	echo '<input type="textarea" id="iut-ingredients" name="iut-ingredients" value="' . $ingredients . '">';
	echo '</label>';
	echo '</p>';
}

// Save post meta
function iut_save_post( $post_id ) {

	if ( isset( $_POST['iut-ingredients'] )) {

		update_post_meta(
			$post_id,
			'iut-ingredients',
			sanitize_text_field( $_POST['iut-ingredients'] )
		);
	}

}

add_action( 'save_post', 'iut_save_post' );
